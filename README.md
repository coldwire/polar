# Polar
## Decentralized, resilient storage network

Polar aim to be a resilient, decentralized and independent storage network as a base for anonymous and e2e encrypted services such a Bloc or Disperse (Cloud storage and collaborative tools).
## Architecture

The project is built on top of many different modules which are:
* **Binaries**:
  * **polarctl**: just a CLI for managing polard using it's internal API
  * **polard**: polard is the background service for running a polar instance

* **Libraries**:
  * **event_manager**: The core library which allow us to easily pass events from a module to another one
  * **management_api**: An administration API, it can be used to ban others nodes, to register services, etc.
  * **services_api**: The internal API for others services of the provider to interact with polar (bloc, disperse, etc). To use it a service must be register using polarctl which will return an authentication token.
  * **node_api**: This is the public node API that'll be used by others nodes to register themselves, upload / download / update objects, etc.
  * **user_api**: The user API allow humans to interact with the network, authenticate themselves, upload / download / update theirs objects, etc.
  * **connector**: To send request to other nodes.
  * **vault**: Since every nodes have their own key pair for signing requests we need to safely store the private key and wrap functions from liboxyd for helping the development.
  * **storage**: Just an interface to abstract the storage layer, actually we only support filesystem and S3.
  * **database**: Simple interface to abstract the database layer, simplify migrations, connections, etc
  * **models**: This library is here to have a central place for all data models, to avoid mistakes in the code and facilitate its updates. 

You can see the schematic just below:
![Architecture](architecture.png)