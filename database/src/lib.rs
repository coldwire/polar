use models::config::{DatabaseConfig};
use sqlx::{postgres::PgConnectOptions, Pool, Postgres};

pub async fn init(config: DatabaseConfig) -> Pool<Postgres> {
    let options = PgConnectOptions::new()
        .host(&config.postgres.host)
        .port(config.postgres.port)
        .username(&config.postgres.username)
        .password(&config.postgres.password)
        .database(&config.postgres.database);

    Pool::<Postgres>::connect_with(options).await.unwrap()
}